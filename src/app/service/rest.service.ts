import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Conference } from '../domain/conference';
import { ConferenceRoom } from '../domain/conference-room';
import { Participant } from '../domain/participant';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

const REST_GET_CONFERENCE = 'http://localhost:8080/api/conference';
const REST_CREATE_CONFERENCE = 'http://localhost:8080/api/conference/create';
const REST_GET_ROOMS = 'http://localhost:8080/api/room';

@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(private http: HttpClient) { }

  getConferences(): Observable<Conference[]> {
    return this.http.get<Conference[]>(REST_GET_CONFERENCE);
  }

  getConferenceById(id: number): Observable<Conference> {
    return this.http.get<Conference>(`${REST_GET_CONFERENCE}/${id}`);
  }

  saveConference(conf: Conference, update: boolean): Observable<Conference> {
    const body = JSON.stringify(conf);
    if (update) {
      return this.http.put<Conference>(`${REST_GET_CONFERENCE}/${conf.id}/update`, body, httpOptions);
    }
    return this.http.post<Conference>(REST_CREATE_CONFERENCE, body, httpOptions);
  }

  deleteConference(id: number): Observable<any> {
    return this.http.delete<any>(`${REST_GET_CONFERENCE}/${id}`);
  }

  getParticipantsByConference(confId: number): Observable<Participant[]> {
    return this.http.get<Participant[]>(`${REST_GET_CONFERENCE}/${confId}/participant`);
  }

  addParticipant(confId: number, participant: Participant): Observable<Participant> {
    return this.http.post<Participant>(`${REST_GET_CONFERENCE}/${confId}/participant`, participant, httpOptions);
  }

  removeParticipant(confId: number, id: number): Observable<any> {
    return this.http.delete<any>(`${REST_GET_CONFERENCE}/${confId}/participant/${id}`);
  }

  getRooms(): Observable<ConferenceRoom[]> {
    return this.http.get<ConferenceRoom[]>(REST_GET_ROOMS);
  }
}
