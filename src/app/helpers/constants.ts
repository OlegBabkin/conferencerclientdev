/**
 * Debug mode
 */
export const DEBUG = true;

/**
 * Titles and descriptions
 */
export const CONFERENCE_VIEW_TITLE = 'Conferences list';
export const CONFERENCE_VIEW_DESC = 'Here you can delete and manage each conference from the list. ' +
                                    'Feel free to schedule a new conference if needed.';

/**
 * RestAPI endpoints
 */
export const REST_GET_CONFERENCE = '/api/conference';
