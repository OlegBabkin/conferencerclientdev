import { Component, OnInit } from '@angular/core';
import { CONFERENCE_VIEW_TITLE, CONFERENCE_VIEW_DESC } from '../../../helpers/constants';
import { Conference } from '../../../domain/conference';

@Component({
  selector: 'app-conference-view',
  templateUrl: './conference-view.component.html',
  styleUrls: ['./conference-view.component.scss']
})
export class ConferenceViewComponent implements OnInit {

  title = CONFERENCE_VIEW_TITLE;
  description = CONFERENCE_VIEW_DESC;

  conference: Conference;

  tabs = [];

  constructor() { }

  ngOnInit() {
  }

  addTab(value: any) {
    const n = typeof value === 'object' ? value.name : value;
    this.conference = typeof value === 'object' ? value : undefined;
    this.tabs.push(n);
  }

  removeTab(index: number) {
    this.tabs.splice(index, 1);
  }

}
