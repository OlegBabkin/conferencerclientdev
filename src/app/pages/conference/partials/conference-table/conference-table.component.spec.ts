import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConferenceTableComponent } from './conference-table.component';

describe('ConferenceTableComponent', () => {
  let component: ConferenceTableComponent;
  let fixture: ComponentFixture<ConferenceTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConferenceTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConferenceTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
