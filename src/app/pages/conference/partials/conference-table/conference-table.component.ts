import { Component, OnInit, ViewChild, AfterViewInit, Output, EventEmitter, Input, OnChanges, SimpleChanges } from '@angular/core';
import { MatTableDataSource, MatSort } from '@angular/material';
import { Conference } from '../../../../domain/conference';
import { RestService } from '../../../../service/rest.service';

@Component({
  selector: 'app-conference-table',
  templateUrl: './conference-table.component.html',
  styleUrls: ['./conference-table.component.scss']
})
export class ConferenceTableComponent implements AfterViewInit, OnInit, OnChanges {

  dataSource: MatTableDataSource<Conference>;

  @Input() isActive: boolean;
  @Output() update = new EventEmitter();

  // All columns of the table
  columns = ['dateTime', 'name', 'room', 'location', 'actions' ];

  // Columns that needed to be defined separately
  excludeColumns = [ 'dateTime', 'actions', 'room', 'location' ];

  @ViewChild('conferenceTableSort') conferenceTableSort: MatSort;

  constructor(private rest: RestService) {
    this.dataSource = new MatTableDataSource();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.isActive.currentValue) {
      this.refreshData();
    }
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.conferenceTableSort;
  }

  refreshData() {
    this.rest.getConferences().subscribe(
      res => this.dataSource.data = res,
      err => console.error(err)
    );
  }

  onUpdate(conf: Conference) {
    this.update.emit(conf);
  }

  onDelete(id: number) {
    if (confirm('Are you sure to delete conference? All registered participants will also be deleted!')) {
      this.rest.deleteConference(id).subscribe(
        res => this.refreshData(),
        err => console.error('ERROR: ', err)
      );
    }
  }
}
