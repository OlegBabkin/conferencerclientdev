import { Component, OnInit, Input, ViewChild, AfterViewInit } from '@angular/core';
import { MatSort, MatTableDataSource } from '@angular/material';
import { Participant } from '../../../../../domain/participant';
import { RestService } from '../../../../../service/rest.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Conference } from '../../../../../domain/conference';

@Component({
  selector: 'app-participant-table',
  templateUrl: './participant-table.component.html',
  styleUrls: ['./participant-table.component.scss']
})
export class ParticipantTableComponent implements OnInit, AfterViewInit {

  columns = ['name', 'birthDate', 'actions' ];
  excludeColumns = [ 'birthDate', 'actions' ];
  dataSource: MatTableDataSource<Participant>;
  participantForm: FormGroup;
  conference: Conference;
  loaded = false;

  @Input() confId: number;
  @ViewChild('participantTableSort') participantTableSort: MatSort;

  constructor(private rest: RestService, private fb: FormBuilder) {
    this.dataSource = new MatTableDataSource();
  }

  ngOnInit() {
    this.rest.getConferenceById(this.confId).subscribe(
      res => {
        this.conference = res;
        this.loaded = true;
      },
      err => console.error(err)
    );
    this.refreshData();
    this.participantForm = this.fb.group({
      id: '',
      name: ['', Validators.required],
      birthDate: ''
    });
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.participantTableSort;
  }

  refreshData() {
    this.rest.getParticipantsByConference(this.confId).subscribe(
      res => this.dataSource.data = res,
      err => console.error(err)
    );
  }

  onAdd() {
    this.rest.addParticipant(this.confId, this.participantForm.value).subscribe(
      res => this.refreshData(),
      err => console.error(err)
    );
    this.participantForm.reset();
  }

  onRemove(id: number) {
    this.rest.removeParticipant(this.confId, id).subscribe(
      res => this.refreshData(),
      err => console.error(err)
    );
  }
}
