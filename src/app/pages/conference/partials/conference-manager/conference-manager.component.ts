import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ConferenceRoom } from 'src/app/domain/conference-room';
import { RestService } from '../../../../service/rest.service';
import { Conference } from 'src/app/domain/conference';

@Component({
  selector: 'app-conference-manager',
  templateUrl: './conference-manager.component.html',
  styleUrls: ['./conference-manager.component.scss']
})
export class ConferenceManagerComponent implements OnInit {

  conferenceForm: FormGroup;
  rooms: ConferenceRoom[];
  editing = false;

  @Input() id: number;
  @Input() index: number;
  @Output() closeTab = new EventEmitter();

  constructor(private formBuilder: FormBuilder, private rest: RestService) { }

  ngOnInit() {
    this.editing = this.id !== null;
    this.getRooms();

    this.conferenceForm = this.formBuilder.group({
      id: '',
      name: ['', [Validators.required, Validators.maxLength(100)]],
      dateTime: ['', [Validators.required]],
      // timeField: ['', [Validators.required, Validators.pattern('^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$')]],
      conferenceRoom: new FormControl(new ConferenceRoom(), Validators.required)
    });

    if (this.editing) {
      this.rest.getConferenceById(this.id).subscribe(
        res => this.conferenceForm.setValue(res),
        err => console.error('ERROR: ', err));
    }
  }

  getRooms() {
    this.rest.getRooms().subscribe(
      res => this.rooms = res,
      err => console.error('ERROR: ', err)
    );
  }

  compareFn(o1: Conference, o2: Conference): boolean {
    return o1.name === o2.name && o1.id === o2.id;
  }

  onSave() {
    this.rest.saveConference(this.conferenceForm.value, this.editing).subscribe(
      res => {
        this.conferenceForm.setValue(res);
        this.id = res.id;
        this.editing = true;
      },
      err => console.error(err)
    );
  }

  onClose() {
    this.closeTab.emit(this.index);
  }

}
