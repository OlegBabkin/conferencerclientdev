import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material';
import { WavesModule, ButtonsModule } from 'angular-bootstrap-md';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HeaderModule } from './general-components/header/header.module';
import { TableModule } from './general-components/table/table.module';

import { AppComponent } from './app.component';
import { ConferenceViewComponent } from './pages/conference/conference-view/conference-view.component';
import { ConferenceTableComponent } from './pages/conference/partials/conference-table/conference-table.component';
import { ConferenceManagerComponent } from './pages/conference/partials/conference-manager/conference-manager.component';
import { ParticipantTableComponent } from './pages/conference/partials/conference-manager/participant-table/participant-table.component';

@NgModule({
  declarations: [
    AppComponent,
    ConferenceViewComponent,
    ConferenceTableComponent,
    ConferenceManagerComponent,
    ParticipantTableComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MDBBootstrapModule,
    BrowserAnimationsModule,
    MaterialModule,
    WavesModule,
    ButtonsModule,
    FormsModule,
    ReactiveFormsModule,
    HeaderModule,
    TableModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [
    NO_ERRORS_SCHEMA
  ]
})
export class AppModule { }
