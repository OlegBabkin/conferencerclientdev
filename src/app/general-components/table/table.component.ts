import {
  Component,
  OnInit,
  AfterViewInit,
  AfterContentInit,
  ContentChildren,
  ViewChild,
  Input,
  QueryList
} from '@angular/core';
import { MatColumnDef, MatTable, MatPaginator, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent<T> implements OnInit, AfterViewInit, AfterContentInit {

  @ContentChildren(MatColumnDef) columnDefs: QueryList<MatColumnDef>;

  @ViewChild(MatTable) table: MatTable<T>;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  @Input() columns: string[];
  @Input() excludedColumns: string[];
  @Input() dataSource: MatTableDataSource<T>;

  constructor() { }

  ngOnInit() { }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
  }

  ngAfterContentInit(): void {
    this.columnDefs.forEach(columnDef => this.table.addColumnDef(columnDef));
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim().toLowerCase();
    this.dataSource.filter = filterValue;
  }

  excludedColumn(column: string): boolean {
    return this.excludedColumns.some(c => c === column);
  }
}
