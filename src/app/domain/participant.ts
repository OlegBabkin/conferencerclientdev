export interface Participant {
  id: number;
  name: string;
  birthDate: Date;
}
