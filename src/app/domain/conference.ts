import { ConferenceRoom } from './conference-room';

export interface Conference {
  id: number;
  name: string;
  dateTime: Date;
  conferenceRoom: ConferenceRoom;
  // full: boolean;
}
