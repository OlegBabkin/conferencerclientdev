export class ConferenceRoom {
  id: number;
  name: string;
  location: string;
  maxSeats: number;

  constructor() { }
}
