# ConferencerFe

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.1.5. (which is using Webpack 4)

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`.

## Build

Production build located in a <root\dist\conferencer-fe> folder

## Running UI tests

Now UI tests are missing, will be added in a feature.

## Notes

* In a project was used 'Angular Material' and 'MDBootstrap for Angular' libraries.
* Endpoints prefix: http://localhost:8080. 
